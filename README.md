# README

## How to build ansible-minimal

```bash
# fetch latest UBI first
podman pull registry.access.redhat.com/ubi8/ubi
podman build -t cjung/ansible-minimal:0.1.0 -t cjung/ansible-minimal:latest -f ansible/ansible-minimal
```

## How to push ansible-minimal

```bash
# log into registry
podman login registry.gitlab.com
podman push localhost/cjung/ansible-minimal:latest registry.gitlab.com/cjung/podman-examples/ansible-minimal:0.1.0
podman push localhost/cjung/ansible-minimal:latest registry.gitlab.com/cjung/podman-examples/ansible-minimal:latest
```

## How to build ansible-cloud

```bash
podman pull registry.gitlab.com/cjung/podman-examples/ansible-minimal
podman build -t cjung/ansible-cloud:0.1.0 -t cjung/ansible-cloud:latest -f ansible/ansible-cloud
```

## How to push ansible-cloud

```bash
podman login registry.gitlab.com
podman push localhost/cjung/ansible-cloud:latest registry.gitlab.com/cjung/podman-examples/ansible-cloud:0.1.0
podman push localhost/cjung/ansible-cloud:latest registry.gitlab.com/cjung/podman-examples/ansible-cloud:latest
```